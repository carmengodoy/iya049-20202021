import gameObject from "./game_object.js"

//Opciones generales del juego
let data = {
    objects: [],
    options: {
        speedPlayer: 3,
        columns: 12,
        rows: 5,
        areaWidth: 800,
        areaHeight: 600,
        speedBall: 5,
        separation: 35
    }
};

//Variable con booleano para que comience a moverse hasta que se presione espacio
let ball_moving = false;
//Vidas totales que se le daran al jugador
let lifes = 3;
//Variable que ira sumando 1 en cada colision con un bloque
let points = 0;

//Funcion para poner el juego a los valores iniciales
const restartAll = () =>{
    max_p = window.localStorage.getItem('max_points')

    points = 0;
    document.getElementById('game_over').style.visibility = 'hidden';
    data.objects.forEach(e => {
        e.element.remove();
    });

    data.objects = [];
    addPlayer();
    addBall();
    addBlocks();
    addGameZone();
    lifes = 3;
    ball_moving = false;
}

//Funcion para ocultar div de instrucciones
const intructionsHidden = () =>{
    document.getElementById('instructions').style.visibility = 'hidden'; 
}

//Funcion para ocultar div cuando ganas
const winHidden = () =>{
    document.getElementById('ganaste').style.visibility = 'hidden';
}

//Funcion para calcular las dimensiones de la raqueta
const getPlayerDim = () => {
    //Calculamos el alto y ancho de la raqueta
    let w = data.options.areaWidth * 0.15;
    let h = data.options.areaHeight * 0.03;
    return {
        //Calculamos para que la raqueta este centrada en el area de juego
        x: (data.options.areaWidth / 2) - (w / 2),
        y: (data.options.areaHeight) - h,
        w,
        h
    }
}

//Funcion para agregar un objeto
const addObject = (gameObject) => {
    let obj = {
        'go': gameObject,
        'element': document.createElement('div')
    };
    //agragamos el nuevo objeto al array
    data.objects.push(obj);
    var g = document.getElementById('gameDiv')
    //agrega el objeto al html
    g.append(obj.element)
    return obj;
}

//Creamos el area de Juego
const addGameZone = () => {
    //Creamos la zona de juego con las dimensiones que definidas en data.options
    let gameZone  = addObject(new gameObject(0, 0, data.options.areaWidth, data.options.areaHeight));
    //Asignamos color 
    gameZone.go.color = "#272727";
    //Le asignamos que su id sera "gameZone"
    gameZone.element.setAttribute('id', 'gameZone');

    return gameZone;
}

//cremos un array para los bloques y seleccionamos los 5 colores para los mismos
let ArrayBlocks = []
const YELLOW = 'rgb(250, 255, 145)';
const BLUE = 'rgb(69, 87, 247)';
const GREEN = 'rgb(249, 140, 255)';
const PINK = 'rgb(127, 245, 159)';
const RED = 'rgb(255, 46, 81)';

//Creamos los bloques
const addBlocks = () => {
    //Vaciamos el array                  
    ArrayBlocks=[];
    //Metemos los colores decididos en un array
    let blockColors = [RED,BLUE ,YELLOW ,PINK ,GREEN];

    //Vamos a pintar los bloques con las configuraciones de filas y columnas que tenemos en data.options
    for (let x = 0; x < game.data.options.columns; x++) {
        for (let y = 0; y < game.data.options.rows; y++) {
            let op = game.data.options;
            //Asignamos ancho y alto para cada bloque
            let ancho = (op.areaWidth - game.data.options.separation * 2) / op.columns;
            let alto = (op.areaHeight * 0.2) / op.rows;
            //Creamos cada bloque teniendo en cuenta la separacion del borde superior que tenemos en data.options
            let block = game.methods.addObject(new game.gameObject(x * ancho + game.data.options.separation, y * alto + game.data.options.separation, ancho, alto));
            //Asignamos un color aleatorio de nuestra seleccion a los bloques
            block.go.color = blockColors[Math.floor(Math.random() * blockColors.length)]
            //Agregamos cada bloque al array
            ArrayBlocks.push(block);
        }
    }
}

//Crear la raqueta que sera nuestro jugador
let player = null;
const addPlayer = () => {
    
    let playerDim = getPlayerDim();
    //Creamos al jugador con las dimensiones definidas en el metodo getPlayerDim()
    player = addObject(new game.gameObject(playerDim.x, playerDim.y, playerDim.w, playerDim.h));
    //Asignamos los estilos que queremos para nuestra raqueta
    player.go.color = 'rgb(192, 192, 192)';
    player.element.style.borderWidth = "1px"
    player.element.style.borderRadius = '3px'
    return player;
}

//Crear Bola
let ball = null;
const addBall = () => {
    //ancho y alto de la pelotax    
    let w_ball = 15;
    let h_ball = 15;
    //Posicionamos la bola en el centro de nuestra raqueta
    let x_ball = (player.go.x + (player.go.width / 2)) - (w_ball / 2);
    let y_ball = player.go.y - h_ball * 1.25;

    //Creamos la pelota con las dimensiones y posicion definida
    ball = addObject(new gameObject(x_ball, y_ball, w_ball, h_ball));
   
    //Asignamos los estilos a la pelota
    ball.element.style.borderRadius = '25px'
    ball.element.style.borderWidth = '1px'
    //Le asignamos que su id sera ''ball''
    ball.element.setAttribute('id', 'ball');
    //Velocidad inicial pelota
    ball.go.speed = {
        x: -3,
        y: -3
    }
    return ball;
}

//Variable para guardar el maximo puntaje
let max_p = 0;
//Funcion con la que obtenemos el maximo puntaje
const loadPoints = () => {
    //Obtenemos el maximo puntaje almacenado en localStorage
    max_p = window.localStorage.getItem('max_points')
    //Comprobamos si el puntaje actual es mayor que el maximo puntaje
    if(points > max_p){
    //Si es mayor le damos a max_points el valor del puntaje
    window.localStorage.setItem('max_points', points) 
    }
}

//Para el movimiento de nuestro jugador
let keyLeft = false;
let keyRight = false;
let playerDirection = 0;

//Eventos con el teclado
const keyDown = (event) => {
   // console.log(event.keyCode);
    switch (event.keyCode) {
        case 37: //Izquierda
            keyLeft = true;
            break;
        case 39: //Derecha
            keyRight = true;
            break;
        case 32: //Espacio
        //Comprobamos que el div de las instrucciones ya no se este mostrando
            if(lifes > 0 && (document. getElementById('instructions').style.visibility == 'hidden')){
                ball_moving = true;
            }
            break;
        case 82: // R para refrescar
        restartAll();
        break;  
        default:
            break;
    }
}
//Eventos con el teclado
const keyUp = (event) => {
    switch (event.keyCode) {
        case 37: //Izquierda
            keyLeft = false;
            break;
        case 39: //Derecha
            keyRight = false;
            break;
        case 32: //Espacio
            console.log(window.game);
            break;
        default:
            break;
    }
}

//Calculamos el Vector del angulo para luego calcular la direccion de la pelota
const calcVectorFromAngle = (angle) => {
    let rad = angle * Math.PI / 180;
    let x = Math.cos(rad);
    let y = Math.sin(rad);
    return {
        x,
        y
    }
}

//Funcion para detectar colision entre dos objetos
const collisionDetection = (obj1, obj2) => {
    //hacemos cuatro comprobaciones
    return (obj1.go.x < obj2.go.x + obj2.go.width && //La posicion en x del obj1 sea menor que la posicion en x del obj2 mas el ancho del obj2
            obj1.go.x + obj1.go.width > obj2.go.x &&  //La posicion en x del obj1 mas el ancho del obj1 sea mayor que la posicion del obj2
            obj1.go.y < obj2.go.y + obj2.go.height && //La posicion en y del obj1 sea menor que la posicion en y del obj2 mas el alto del obj2
            obj1.go.height + obj1.go.y > obj2.go.y) //El alto del obj1 mas la posicion del obj1 en y sea mayor que la posicion en y del obj2
}

//Funcion que contiene todo lo que se estara actualizando constantemente
const update = () => {
    data.objects.forEach(e => {
        //para todos los objetos creados asignamos los siguientes estilos
        e.element.style.position = "absolute";
        e.element.style.height = e.go.height + 'px';
        e.element.style.width = e.go.width + 'px';
        e.element.style.top = e.go.y + 'px';
        e.element.style.left = e.go.x + 'px';
        e.element.style.borderStyle = 'solid';
        //propiedad que de los bloques para color
        e.element.style.backgroundColor = e.go.color;
    });

    //Saber que esta chocando con el area de juego en x
    let collision_left = ball.go.x < 0;
    let collision_right = ball.go.x > (data.options.areaWidth - ball.go.width);
    //Saber que esta chocando con el area de juego en y
    let collision_up = ball.go.y < 0;
    //Saber si pierde una vida
    let collision_down = ball.go.y > (data.options.areaHeight - ball.go.height);

    //Condicionales para cambiar direccion de pelota cuando hay una colision
    if (collision_left || collision_right) {
        ball.go.speed.x = -ball.go.speed.x;
    }
    if (collision_up || collision_down) {
        ball.go.speed.y = -ball.go.speed.y;
    }

    //Cuando se tectecta una colision en y inferior
    if(collision_down){
        //paramos la pelota
        ball_moving = false;
        //restamos una vida
        lifes--;
        //Si vidas es igual a cero 
        if(lifes == 0){
            //Mostramos el div de game over
            document.getElementById('game_over').style.visibility = 'visible';
            loadPoints();

        }
      
    }
   
    //Para cada obj en ArrayBlocks
    ArrayBlocks.forEach(obj => {
        //Detectamos colision de la pelota con los bloques
        if (collisionDetection(obj, ball)) {
            //Si colisiona con un bloque rojo
            if(obj.element.style.backgroundColor === RED ){
                //Cambiamos el color a azul
                obj.go.color = BLUE;
                //Sumamos un punto
                points ++;
            //Si colisiona con un bloque azul
            }else if(obj.element.style.backgroundColor === BLUE){
                //Cambiammos el color a amarillo
                obj.go.color = YELLOW;
                //Sumamos un punto
                points ++;
            }else{
                //Si colisiona con uno de cualquier otro color lo eliminamos del array
                ArrayBlocks.splice(ArrayBlocks.indexOf(obj), 1);
                //Lo quitamos de la vista
                obj.element.style.visibility = 'hidden';
                //Sumamos un punto
                points ++;
                //Cuando la longitud del array sea 0
                if(ArrayBlocks.length == 0){
                    //Pausamos la pelota
                    ball_moving = false;
                    //Mostramos el div Ganaste 
                    document.getElementById('ganaste').style.visibility = 'visible';
                }
            }

            let y_height = obj.go.y + obj.go.height;
            let y = obj.go.y;
            let x_width = obj.go.x + obj.go.width;
            let x = obj.go.x;
            let ball_center_x = ball.go.x + 5;
            let ball_center_y = ball.go.y + 5;
            //arriba
            if (ball_center_y > y && ball_center_y > y_height) {
                ball.go.speed.y = -ball.go.speed.y;

                //Abajo
            } else if (ball_center_y < y && ball_center_y < y_height) {
                ball.go.speed.y = -ball.go.speed.y;
                //izquierda
            } else if (ball_center_x < y && ball_center_x < x_width) {
                ball.go.speed.x = -ball.go.speed.x;
                //derecha
            } else if (ball_center_x > y && ball_center_x > x_width) {
                ball.go.speed.x = -ball.go.speed.x;
            }


        }

    })

    //Detectamos colision de la pelota con la raqueta
    if (collisionDetection(ball, player)) {

        //Variables para calcular posiciones
        let player_x = player.go.x + (player.go.width / 2);
        let ball_x = ball.go.x + 5;

        let player_l = player_x - player.go.width / 2;

        //Distancia entre la pelota y el jugador
        let distance_P_B = ball_x - player_l;
        //pasamos la distancia a porcentaje en base al tamaño del jugador
        let angle = (180 * distance_P_B) / player.go.width;

        //Si el angulo es demasiado pequeño
        if(angle < 20){
            //le damos el valor de 20 para que el angulo de rebote no sea demasiado horizontal
            angle = 20;
        //Si el angulo es demasiado grande
        }else if(angle>160){
            //le damos el valor de 160 para que el angulo de rebote no sea demasiado horizontal
            angle = 160;
        }

        //ponemos la nueva direccion de nuestra pelota despues de chocar con nuestro player
        let newSpeedX = calcVectorFromAngle(angle).x * -1 * data.options.speedBall;
        let newSpeedY = calcVectorFromAngle(angle).y * -1 * data.options.speedBall;
        ball.go.speed.x = newSpeedX;
        ball.go.speed.y = newSpeedY;
    }

    //Para que nuestro jugador se mueva constantemente y que no se salga de nuestro recuadro de juego 
    if (player.go.x > 0 && keyLeft) {
        data.objects[0].go.moveX(-1 * data.options.speedPlayer)
    }
    if (player.go.x < (data.options.areaWidth - player.go.width) && keyRight) {
        data.objects[0].go.moveX(1 * data.options.speedPlayer)
    }
    //movimiento de la bola
    if (ball_moving)  {
        ball.go.x += ball.go.speed.x;
        ball.go.y += ball.go.speed.y;
    }else{
        //para que pelota se mueva junto a la raqueta 
        let w_ball = 15;
        let h_ball = 15;
        let x_ball = (player.go.x + (player.go.width / 2)) - (w_ball / 2);
        let y_ball = player.go.y - h_ball * 1
        ball.go.x = x_ball;
        ball.go.y = y_ball;
    }

    //Añadimos los puntajes y las vidas a nuestro html
    document.getElementById('lifes').innerHTML = lifes;
    document.getElementById('points').innerHTML = points;
    let elements = document.getElementsByClassName('maxPoints')
    for(let i=0; i<elements.length; i++) {
        elements[i].innerHTML = window.localStorage.getItem('max_points');
    }
    
}

const game = {
    gameObject,
    data,
    methods: {
        update,
        addObject,
        keyDown,
        keyUp,
        getPlayerDim,
        addPlayer,
        addGameZone,
        addBall,
        addBlocks,
        restartAll,
        intructionsHidden,
        winHidden

    }
};

export default game;